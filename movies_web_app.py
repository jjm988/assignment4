import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application




def get_db_creds():
    # db = 'testdb'
    # username = 'root'
    # password = 'testpass123!@#'
    # hostname = '34.69.221.248'
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year VARCHAR(4) NULL, title VARCHAR(45) NULL, director VARCHAR(45) NULL, actor VARCHAR(45) NULL,release_date VARCHAR(20) NULL, rating FLOAT NULL, PRIMARY KEY (id), UNIQUE INDEX(title))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    year = '2001'
    title = '2012'
    director = 'Brian'
    actor = 'Amanda'
    release_date = '23 Nov 2009'
    rating = 5.9
    cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) values ('" + year + "', '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', " + str(rating) +  ")")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title FROM movies")
    entries = [dict(title=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")
    #print(request.form['movies'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    msg = 'I am a message'

    check_count = "SELECT COUNT(title) FROM movies WHERE title = '" + title + "'"

    try:
        cur.execute(check_count)
        count = cur.fetchone()[0]
        if count != 0:
            msg = 'Movie with ' + title + ' already exists'
            return render_template('index.html', message=msg)
        else:
            cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) values ('" + year + "', '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', " + str(rating) +  ")")
            cnx.commit()
    except mysql.connector.Error as err:
        msg = 'movie ' + title + ' could not be inserted - ' + err.msg
    else:
        msg = 'movie ' + title + ' successfully inserted'

    return render_template('index.html', message=msg)


@app.route('/update_movie', methods=['POST'])
def update_to_db():
    print("Received request.")
    #print(request.form['movies'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    msg = 'I am a message'
    check_exist = "SELECT COUNT(title) FROM movies WHERE title = '" + title + "'"
    try:
        cur.execute(check_exist)
        count = cur.fetchone()[0]
        if count == 0:
            msg = 'movie ' + title + ' could not be updated - movie does not exist'
            return render_template('index.html', message=msg)
    except mysql.connector.Error as err:
        msg = err.msg

    try:
        cur.execute("UPDATE movies SET year = '" + year + "', director = '" + director + "', actor = '" + actor + "', release_date = '" + release_date + "', rating = '" + rating + "' WHERE title = '" + title + "'")
        cnx.commit()
    except mysql.connector.Error as err:
        msg = 'movie ' + title + ' could not be updated - ' + err.msg
    else:
        msg = 'movie ' + title + ' successfully updated'

    return render_template('index.html', message=msg)

@app.route('/delete_movie', methods=['POST'])
def delete_from_db():
    print("Received request.")
    #print(request.form['movies'])
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    msg = ''
    check_exist = "SELECT COUNT(title) FROM movies WHERE title = '" + title + "'"
    try:
        cur.execute(check_exist)
        count = cur.fetchone()[0]
        if count == 0:
            msg = 'Movie with ' + title + ' does not exist'
            return render_template('index.html', message=msg)
        else:
            cur.execute("DELETE FROM movies WHERE Upper(title) = '" + title.upper() + "'")
            cnx.commit()
            msg = 'movie ' + title + ' successfully deleted'
    except mysql.connector.Error as err:
        msg = 'movie ' + title + ' could not be deleted - ' + err.msg
    return render_template('index.html', message=msg)


@app.route('/search_movie', methods=['POST'])
def search_db():
    print("Received request.")
    actor = request.form['search_actor']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    msg = ''

    try:
        cur.execute("SELECT title, year, actor from movies WHERE Upper(actor) = '" + actor.upper() + "'")
        rows = cur.fetchall()
    except mysql.connector.Error as err:
        msg = err.msg

    if len(rows) == 0:
        msg = 'No movies found for actor ' + actor
        return render_template('index.html', message=msg)
    else:

        for row in rows:
            msg += "{}, {}, {} <br>".format(row[0], row[1], row[2])
        return render_template('index.html', message=msg)

@app.route('/highest_rating', methods=['POST'])
def get_highest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    msg = ''
    query = "SELECT m.title, m.year, m.actor, m.director, m.rating FROM movies m INNER JOIN(SELECT MAX(rating) as max_rating FROM movies)b on m.rating = b.max_rating"
    try:
        cur.execute(query)
        rows = cur.fetchall()
    except mysql.connector.Error as err:
        return render_template('index.html', message=err.msg)

    for row in rows:
        msg += '{} {} {} {} {} <br>'.format(row[0], row[1], row[2], row[3], row[4])

    return render_template('index.html', message=msg)


@app.route('/lowest_rating', methods=['POST'])
def get_lowest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    msg = ''
    query = "SELECT m.title, m.year, m.actor, m.director, m.rating FROM movies m INNER JOIN(SELECT MIN(rating) as min_rating FROM movies)b on m.rating = b.min_rating"
    try:
        cur.execute(query)
        rows = cur.fetchall()
    except mysql.connector.Error as err:
        return render_template('index.html', message=err.msg)

    for row in rows:
        msg += '{} {} {} {} {} <br>'.format(row[0], row[1], row[2], row[3], row[4])

    return render_template('index.html', message=msg)


@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
